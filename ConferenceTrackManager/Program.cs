﻿namespace ConferenceTrackManager
{
    internal static class Program
    {
        private static void Main()
        {
            MainController mainController = new MainController();
            
            mainController.Start();
        }
    }
}