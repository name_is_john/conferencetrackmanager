using System.Collections.Generic;
using ConferenceTrackManager.Domain;
using FileInputOutput;
using TrackScheduler;

namespace ConferenceTrackManager
{
    public class MainController
    {
        private readonly string filePath = @"../TalkList.txt";
        private string[] textFromFile;
        private List<ConferenceTalk> conferenceTalkList;
        private List<ConferenceTrack> trackList;
        private ConferenceTrack conferenceTrack;
        private TrackBuilder trackBuilder;
        private ConferenceFileReader conferenceFileReader;
        private ConferenceScheduleWriter conferenceScheduleWriter;

        public MainController()
        {
            conferenceTalkList = new List<ConferenceTalk>();
            conferenceTrack = new ConferenceTrack();
            trackBuilder = new TrackBuilder(conferenceTrack);
            conferenceFileReader = new ConferenceFileReader(filePath);
            conferenceScheduleWriter = new ConferenceScheduleWriter();
        }

        public void Start()
        {
            textFromFile = conferenceFileReader.Read();

            var conferenceFileParser = new ConferenceFileParser(textFromFile);
            conferenceTalkList = conferenceFileParser.PopulateConferenceTalkList();
            
            var scheduler = new Scheduler(conferenceTalkList, trackBuilder);
            trackList = scheduler.PopulateConferenceTrackList();
 
            conferenceScheduleWriter.WriteToConsole(trackList);
        }
    }
}