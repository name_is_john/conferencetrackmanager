using System;
using System.Collections.Generic;
using ConferenceTrackManager.Domain;
using ConferenceTrackManager.Domain.Enums;
using TrackScheduler.Interfaces;

namespace TrackScheduler
{
    public class TrackBuilder : ITrackBuilder<ConferenceTrack>
    {
        private ConferenceTrack conferenceTrack;

        public TrackBuilder(ConferenceTrack conferenceTrack)
        {
            this.conferenceTrack = conferenceTrack;
        }

        public ConferenceTrack Build()
        {
            conferenceTrack = IsPreviousTrackMorning() ? createAfternoonConferenceTrack() : createMorningConferenceTrack();

            return conferenceTrack;
        }

        private ConferenceTrack createMorningConferenceTrack()
        {
            return new ConferenceTrack
            {
                ConferenceTalks = new List<ConferenceTalk>(),
                CapacityInMinutes = 180,
                SumOfTalks = 0,
                Session = Session.Morning,
                IsFull = false,
                StartTime = DateTime.Today.AddHours(9),
                NonTalkEvents = NonTalkEvents.Lunch
            };
        }

        private ConferenceTrack createAfternoonConferenceTrack()
        {
            return new ConferenceTrack
            {
                ConferenceTalks = new List<ConferenceTalk>(),
                CapacityInMinutes = 240,
                SumOfTalks = 0,
                Session = Session.Afternoon,
                IsFull = false,
                StartTime = DateTime.Today.AddHours(13),
                NonTalkEvents = NonTalkEvents.Networking
            };
        }

        private bool IsPreviousTrackMorning()
        {
            return conferenceTrack.Session == Session.Morning;
        }
    }
}