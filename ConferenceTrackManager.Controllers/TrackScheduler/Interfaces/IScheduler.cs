using System.Collections.Generic;
using ConferenceTrackManager.Domain;

namespace TrackScheduler.Interfaces
{
    public interface IScheduler<T>
    {
        List<ConferenceTrack> PopulateConferenceTrackList();
    }
}