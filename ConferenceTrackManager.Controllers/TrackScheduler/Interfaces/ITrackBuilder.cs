namespace TrackScheduler.Interfaces
{
    public interface ITrackBuilder<T>
    {
        T Build();
    }
}