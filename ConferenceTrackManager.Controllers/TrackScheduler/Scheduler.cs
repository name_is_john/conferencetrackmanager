﻿using System.Collections.Generic;
using ConferenceTrackManager.Domain;
using TrackScheduler.Interfaces;

namespace TrackScheduler
{
    public class Scheduler : IScheduler<ConferenceTalk>
    {
        private List<ConferenceTrack> conferenceTrackList = new List<ConferenceTrack>();
        private readonly List<ConferenceTalk> conferenceTalkList;
        private ITrackBuilder<ConferenceTrack> trackBuilder;
        private int numberOfTalks;

        public Scheduler(List<ConferenceTalk> conferenceTalkList, ITrackBuilder<ConferenceTrack> trackBuilder)
        {
            this.conferenceTalkList = conferenceTalkList;
            this.trackBuilder = trackBuilder;
        }

        public List<ConferenceTrack> PopulateConferenceTrackList()
        {
            var conferenceTrack = trackBuilder.Build();
            
            foreach (var talk in conferenceTalkList)
            {
                numberOfTalks += 1;
                if (!IsTrackAtCapacity(talk, conferenceTrack))
                {
                    if (IsTrackListComplete())
                    {
                        conferenceTrackList?.Add(conferenceTrack);
                    }
                }
                else
                {
                    conferenceTrackList?.Add(conferenceTrack);
                    conferenceTrack = trackBuilder.Build();
                }

                conferenceTrack.ConferenceTalks?.Add(talk);
                conferenceTrack.SumOfTalks += talk.DurationInMinutes;
            }

            return conferenceTrackList;
        }

        private bool IsTrackListComplete()
        {
            return conferenceTalkList.Count == numberOfTalks;
        }

        private bool IsTrackAtCapacity(ConferenceTalk talk, ConferenceTrack conferenceTrack)
        {
            if (IsTalkSumLessThanCapacity(talk, conferenceTrack))
            {
                conferenceTrack.IsFull = false;
            }
            else
            {
                conferenceTrack.IsFull = true;
            }

            return conferenceTrack.IsFull;
        }

        private bool IsTalkSumLessThanCapacity(ConferenceTalk talk, ConferenceTrack conferenceTrack)
        {
            return conferenceTrack.SumOfTalks + talk.DurationInMinutes <= conferenceTrack.CapacityInMinutes;
        }
    }
}