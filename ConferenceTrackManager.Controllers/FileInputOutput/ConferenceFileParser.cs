﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ConferenceTrackManager.Domain;
using FileInputOutput.Interfaces;

namespace FileInputOutput
{
    public class ConferenceFileParser : IFileParser
    {
        private readonly string[] textFromFile;
        private List<ConferenceTalk> conferenceInformationList;
        private readonly string parserPattern = @"(\d+)";
        private readonly string lightningPattern = "(\\s+(lightning)\\s*)$";
        private readonly string lightningRoundTimeInMinutes = "5";
        string durationOfPresentationInMinutes;
        string topicOfPresentation;

        public ConferenceFileParser(string[] textFromFile)
        {
            this.textFromFile = textFromFile;
            conferenceInformationList = new List<ConferenceTalk>();
        }

        public List<ConferenceTalk> PopulateConferenceTalkList()
        {
            foreach (string lineOfText in textFromFile)
            {
                var conferenceInformation = GatherConferenceTalk(lineOfText);

                conferenceInformationList.Add(conferenceInformation);
            }

            return conferenceInformationList;
        }

        private ConferenceTalk GatherConferenceTalk(string lineOfContent)
        {
            string[] parsedTalkInformation = SplitLineOfTextAtDigit(lineOfContent);

            if (LineIsSplit(parsedTalkInformation))
            {
                topicOfPresentation = parsedTalkInformation[0].Trim();
                durationOfPresentationInMinutes = parsedTalkInformation[1].Trim();
            }
            else if (LineIsNotSplit(parsedTalkInformation))
            {
                var topicWithLightningRemoved = RemoveLightningFromText(parsedTalkInformation);
                
                topicOfPresentation = topicWithLightningRemoved;
                durationOfPresentationInMinutes = lightningRoundTimeInMinutes;
            }
            else
            {
                Console.WriteLine("Error parsing text input");
            }

            return new ConferenceTalk
            {
                DurationInMinutes = Convert.ToInt16(durationOfPresentationInMinutes),
                Title = topicOfPresentation
            };
        }

        private string[] SplitLineOfTextAtDigit(string lineOfContent)
        {
            string[] splitLineOfText = { lineOfContent };

            try
            {
                splitLineOfText = Regex.Split(splitLineOfText[0], parserPattern);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed to Regex line of text with exception: {exception}");
            }

            return splitLineOfText;
        }
        
        private static bool LineIsSplit(string[] informationFromStringInput)
        {
            return informationFromStringInput.Length >= 2;
        }
        
        private static bool LineIsNotSplit(string[] informationFromStringInput)
        {
            return informationFromStringInput.Length == 1;
        }
        
        private string RemoveLightningFromText(string[] informationFromStringInput)
        {
            var regex = new Regex(lightningPattern);
            
            return regex.Replace(informationFromStringInput[0], string.Empty);
        }
    }
}