﻿using System.Collections.Generic;
using ConferenceTrackManager.Domain;

namespace FileInputOutput.Interfaces
{
    public interface IFileParser
    {
        List<ConferenceTalk> PopulateConferenceTalkList();
    }
}