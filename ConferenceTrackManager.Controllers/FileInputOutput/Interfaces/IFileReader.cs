﻿using System.Collections.Generic;

namespace FileInputOutput.Interfaces
{
    public interface IFileReader
    {
        string[] Read();
    }
}