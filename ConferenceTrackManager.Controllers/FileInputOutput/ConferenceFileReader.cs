﻿using System;
using System.Collections.Generic;
using System.IO;
using FileInputOutput.Interfaces;

namespace FileInputOutput
{
    public class ConferenceFileReader : IFileReader
    {
        private readonly string filePath;
        private string[] textFromFile;

        public ConferenceFileReader(string filePath)
        {
            this.filePath = filePath;
        }

        public string[] Read()
        {
            try
            {
                textFromFile = File.ReadAllLines(filePath);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed to read file with the following exception: {exception}");
            }
            
            return textFromFile;
        }
    }
}