using System;
using System.Collections.Generic;
using ConferenceTrackManager.Domain;
using ConferenceTrackManager.Domain.Enums;

namespace FileInputOutput
{
    public class ConferenceScheduleWriter
    {
        public void WriteToConsole(List<ConferenceTrack> trackList)
        {
            Console.WriteLine("Conference Schedule:");
            foreach (ConferenceTrack conferenceTrack in trackList)
            {
                var currentTalkTimeInTrack = 0;
                
                Console.WriteLine($"{conferenceTrack.Session} Track");
                foreach (var talk in conferenceTrack.ConferenceTalks)
                {
                    Console.WriteLine(
                        conferenceTrack.StartTime.AddMinutes(currentTalkTimeInTrack).ToShortTimeString()
                        + " "
                        + talk.Title);
                    
                    currentTalkTimeInTrack += talk.DurationInMinutes;
                }

                DetermineNonTalkEventDisplay(conferenceTrack, conferenceTrack.StartTime);
            }
        }

        private static void DetermineNonTalkEventDisplay(ConferenceTrack conferenceTrack, DateTime trackStartTime)
        {
            if (conferenceTrack.NonTalkEvents == NonTalkEvents.Lunch)
            {
                Console.WriteLine(trackStartTime.AddHours(3).ToShortTimeString() + " " + NonTalkEvents.Lunch);
            }
            else
            {
                Console.WriteLine(trackStartTime.AddHours(4).ToShortTimeString() + " " + NonTalkEvents.Networking);
            }
        }
    }
}