﻿using System;
using System.Collections.Generic;
using ConferenceTrackManager.Domain;
using ConferenceTrackManager.Domain.Enums;
using ConferenceTrackManager.Domain.Interfaces;
using Moq;
using NUnit.Framework;
using TrackScheduler;
using TrackScheduler.Interfaces;

namespace TrackSchedulerTests
{
    [TestFixture]
    public class SchedulerTests
    {
        private Scheduler scheduler;
        List<ConferenceTrack> conferenceTrackList;
        private List<ConferenceTalk> conferenceTalkList;
        private Mock<ITrackBuilder<ConferenceTrack>> mockTrackBuilder;

        [SetUp]
        public void Setup()
        {
            conferenceTalkList = GenerateTestConferenceTalkList();
            conferenceTrackList = new List<ConferenceTrack>();
            mockTrackBuilder = new Mock<ITrackBuilder<ConferenceTrack>>();
            scheduler = new Scheduler(conferenceTalkList, mockTrackBuilder.Object);
        }

        [Test]
        public void PopulateTrack_GivenConferenceTalk_DurationLessThanCapacity_AddsTalkToConferenceTrack()
        {
            var capacityHigherThanFirstTalk = 120;
            mockTrackBuilder.Setup(builder => builder.Build()).Returns(
                SetupConferenceTrack(capacityHigherThanFirstTalk));

            conferenceTrackList = scheduler.PopulateConferenceTrackList();

            CollectionAssert.Contains(conferenceTrackList[0].ConferenceTalks, conferenceTalkList[0]);
        }

        [Test]
        public void PopulateTrack_GivenConferenceTalk_DurationGreaterThanCapacity_AddsConferenceTrackToList_CreatesNewTrack_AddsTalkToNewTrack()
        {
            var capacityLowerThanFirstTalk = 50;
            var capacityHigherThanFirstTalk = 180;
            mockTrackBuilder.SetupSequence(builder => builder.Build())
             .Returns(SetupConferenceTrack(capacityLowerThanFirstTalk))
             .Returns(SetupConferenceTrack(capacityHigherThanFirstTalk));

            conferenceTrackList = scheduler.PopulateConferenceTrackList();

            Assert.True(conferenceTrackList[0].IsFull);
            CollectionAssert.Contains(conferenceTrackList[1].ConferenceTalks, conferenceTalkList[1]);
        }

        [Test]
        public void PopulateTrack_GivenConferenceTalk_Count_Equals_SumOfTalks_AddsConferenceTrackToList()
        {
            var capacityEqualToTalkListTotal = 120;
            mockTrackBuilder.Setup(builder => builder.Build())
                .Returns(SetupConferenceTrack(capacityEqualToTalkListTotal));

            conferenceTrackList = scheduler.PopulateConferenceTrackList();

            Assert.True(conferenceTrackList[0].IsFull);
        }


        private static List<ConferenceTalk> GenerateTestConferenceTalkList()
        {
            List<ConferenceTalk> conferenceTalks = new List<ConferenceTalk>
            {
                new ConferenceTalk
                {
                    DurationInMinutes = 5,
                    Title = "Test Title 1"
                },
                new ConferenceTalk
                {
                    DurationInMinutes = 60,
                    Title = "Test Title 2"
                },
                new ConferenceTalk
                {
                    DurationInMinutes = 30,
                    Title = "Test Title 3"
                },
                new ConferenceTalk
                {
                    DurationInMinutes = 45,
                    Title = "Test Title 4"
                }
            };
            
            return conferenceTalks;
        }
        
        private static ConferenceTrack SetupConferenceTrack(int capacityInMinutes)
        {
            return new ConferenceTrack
            {
                ConferenceTalks = new List<ConferenceTalk>(),
                CapacityInMinutes = capacityInMinutes,
                SumOfTalks = 0,
                Session = Session.Morning,
                IsFull = false,
                StartTime = DateTime.Today.AddHours(9),
                NonTalkEvents = NonTalkEvents.Lunch
            };
        }
    }
}