using ConferenceTrackManager.Domain;
using ConferenceTrackManager.Domain.Enums;
using NUnit.Framework;
using TrackScheduler;

namespace TrackSchedulerTests
{
    [TestFixture]
    public class BuildNewConferenceTrackTests
    {
        private ConferenceTrack conferenceTrack;
        private TrackBuilder trackBuilder;

        [SetUp]
        public void Setup()
        {
            conferenceTrack = new ConferenceTrack();
            trackBuilder = new TrackBuilder(conferenceTrack);
        }

        [Test]
        public void
            CreateNewTrackSession_Given_ConferenceTrackSesion_NotSpecified_Returns_ConferenceTrack_MorningSession()
        {
            conferenceTrack.Session = Session.NotSpecified;

            conferenceTrack = trackBuilder.Build();

            Assert.That(conferenceTrack.Session == Session.Morning);
        }

        [Test]
        public void
            CreateNewTrackSession_Given_ConferenceTrackSesion_Morning_Returns_ConferenceTrack_AfternoonSession()
        {
            conferenceTrack.Session = Session.Morning;

            conferenceTrack = trackBuilder.Build();

            Assert.That(conferenceTrack.Session == Session.Afternoon);
        }

        [Test]
        public void
            CreateNewTrackSession_Given_ConferenceTrackSesion_Afternoon_Returns_ConferenceTrack_MorningSession()
        {
            conferenceTrack.Session = Session.Afternoon;

            conferenceTrack = trackBuilder.Build();

            Assert.That(conferenceTrack.Session == Session.Morning);
        }
    }
}