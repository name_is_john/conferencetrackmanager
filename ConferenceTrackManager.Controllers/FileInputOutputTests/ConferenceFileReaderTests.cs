﻿using FileInputOutput;
using NUnit.Framework;

namespace FileInputOutputTests
{
    // todo: Mark as a Integration Test
    [TestFixture]
    public class ConferenceFileReaderTests
    {
        private ConferenceFileReader conferenceFileReader;
        private string filePath;

        [SetUp]
        public void Setup()
        {
            filePath = @"../../../../../ReadTextTestFile.txt";
            conferenceFileReader = new ConferenceFileReader(filePath);
        }

        [Test]
        public void ReadFromTextFile_Read_ReadsInStringByLine()
        {
            string[] testTextFromFile =
            {
                "Test string 1",
                "Test string 2"
            };

            string[] textFromFile = conferenceFileReader.Read();
            
            StringAssert.Contains(testTextFromFile[0], textFromFile[0]);
            StringAssert.Contains(testTextFromFile[1], textFromFile[1]);
        }
    }
}