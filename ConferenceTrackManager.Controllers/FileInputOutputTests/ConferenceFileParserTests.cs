﻿using System.Collections.Generic;
using Bogus;
using ConferenceTrackManager.Domain;
using FileInputOutput;
using NUnit.Framework;

namespace FileInputOutputTests
{
    [TestFixture]
    public class ConferenceFileParserTests
    {
        private ConferenceFileParser conferenceFileParser;
        private string[] textFromFile;
        private Randomizer randomizer;
        private int randomNumber;
        private string randomText;

        [SetUp]
        public void Setup()
        {
            randomizer = new Randomizer();
            randomNumber = randomizer.Int(1,60);
            randomText = randomizer.Words();
            
            string lineOfTextWithDigit = $"{randomText} {randomNumber} {randomText}";
            string lineOfTextWithoutDigit = $"{randomText}";
            
            textFromFile = new[]
            {
                lineOfTextWithDigit,
                lineOfTextWithoutDigit
            };

            conferenceFileParser = new ConferenceFileParser(textFromFile);
        }

        [Test]
        public void PopulateConferenceTalkList_Returns_ConferenceInformationList()
        {
            ConferenceTalk testConferenceTalk = new ConferenceTalk
            {
                Title = randomText,
                DurationInMinutes = randomNumber
            };

            List<ConferenceTalk> conferenceInformationList =
                conferenceFileParser.PopulateConferenceTalkList();

            Assert.AreEqual(
                testConferenceTalk.Title, 
                conferenceInformationList[0].Title
                );
            Assert.AreEqual(
                testConferenceTalk.DurationInMinutes,
                conferenceInformationList[0].DurationInMinutes
                );
        }

        [Test]
        public void PopulateConferenceTalkList_ReturnsCannedDurationResponse_WhenNoDigitsInTextFileLine()
        {
            int lightningRoundDuration = 5;
            
            ConferenceTalk testConferenceTalk = new ConferenceTalk
            {
                Title = randomText,
                DurationInMinutes = lightningRoundDuration
            };

            List<ConferenceTalk> conferenceInformationList =
                conferenceFileParser.PopulateConferenceTalkList();

            Assert.AreEqual(
                testConferenceTalk.DurationInMinutes, 
                conferenceInformationList[1].DurationInMinutes);
            Assert.AreEqual(
                testConferenceTalk.Title, 
                conferenceInformationList[1].Title);
        }
    }
}