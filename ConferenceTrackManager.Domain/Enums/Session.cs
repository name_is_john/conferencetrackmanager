namespace ConferenceTrackManager.Domain.Enums
{
    public enum Session
    {
        NotSpecified,
        Morning,
        Afternoon
    }
}