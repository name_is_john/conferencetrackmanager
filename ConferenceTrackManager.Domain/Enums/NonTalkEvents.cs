namespace ConferenceTrackManager.Domain.Enums
{
    public enum NonTalkEvents
    {
        Lunch,
        Networking
    }
}