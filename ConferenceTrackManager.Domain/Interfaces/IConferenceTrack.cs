using System;
using System.Collections.Generic;
using ConferenceTrackManager.Domain.Enums;

namespace ConferenceTrackManager.Domain.Interfaces
{
    public interface IConferenceTrack
    {
        List<ConferenceTalk> ConferenceTalks { get; set; }

        double CapacityInMinutes { get; set; }

        double SumOfTalks { get; set; }

        bool IsFull { get; set; }

        Session Session { get; set; }

        DateTime StartTime { get; set; }

        NonTalkEvents NonTalkEvents { get; set; }
    }
}