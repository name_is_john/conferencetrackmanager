namespace ConferenceTrackManager.Domain.Interfaces
{
    public interface IConferenceTalk
    {
        string Title { get; set; }

        double DurationInMinutes { get; set; }    
    }
}