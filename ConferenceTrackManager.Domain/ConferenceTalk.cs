﻿namespace ConferenceTrackManager.Domain
{
    public class ConferenceTalk
    {
        public string Title { get; set; }

        public int DurationInMinutes { get; set; }
    }
}