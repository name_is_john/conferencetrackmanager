using System;
using System.Collections.Generic;
using ConferenceTrackManager.Domain.Enums;

namespace ConferenceTrackManager.Domain
{
    public class ConferenceTrack
    {
        public List<ConferenceTalk> ConferenceTalks { get; set; }

        public double CapacityInMinutes { get; set; }

        public double SumOfTalks { get; set; }

        public bool IsFull { get; set; }

        public Session Session { get; set; }

        public DateTime StartTime { get; set; }

        public NonTalkEvents NonTalkEvents { get; set; }
    }
}